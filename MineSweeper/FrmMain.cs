﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MineSweeper
{
    public partial class FrmMain : Form
    {
        private Point mousePoint = new Point();
        private bool isPressOnWindow = false;
        
        public FrmMain()
        {
            InitializeComponent();
            gameView.OnPublishGameTimeEvent += description =>
            {
                lbGameTime.Text = "当前耗时：" + description;
            };
        }

        private void btnPlayGame_Click(object sender, EventArgs e)
        {
            gameView.ResetGameMap();
        }

        private void FrmMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mousePoint.X = -e.X;
                mousePoint.Y = -e.Y;
                isPressOnWindow = true;
            }
        }

        private void FrmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPressOnWindow)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mousePoint.X, mousePoint.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void FrmMain_MouseUp(object sender, MouseEventArgs e)
        {
            if (isPressOnWindow)
            {
                isPressOnWindow = false;
            }
        }
    }
}
