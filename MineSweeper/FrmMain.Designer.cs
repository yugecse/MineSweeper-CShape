﻿namespace MineSweeper
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnPlayGame = new System.Windows.Forms.Button();
            this.lbGameTime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gameView = new SweeperLibrary.GameView();
            this.SuspendLayout();
            // 
            // btnPlayGame
            // 
            this.btnPlayGame.Location = new System.Drawing.Point(187, 13);
            this.btnPlayGame.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPlayGame.Name = "btnPlayGame";
            this.btnPlayGame.Size = new System.Drawing.Size(100, 91);
            this.btnPlayGame.TabIndex = 1;
            this.btnPlayGame.Text = "开始游戏";
            this.btnPlayGame.UseVisualStyleBackColor = true;
            this.btnPlayGame.Click += new System.EventHandler(this.btnPlayGame_Click);
            // 
            // lbGameTime
            // 
            this.lbGameTime.AutoSize = true;
            this.lbGameTime.Location = new System.Drawing.Point(16, 31);
            this.lbGameTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbGameTime.Name = "lbGameTime";
            this.lbGameTime.Size = new System.Drawing.Size(122, 15);
            this.lbGameTime.TabIndex = 2;
            this.lbGameTime.Text = "当前耗时：00:00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 78);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "总共雷数：10";
            // 
            // gameView
            // 
            this.gameView.BackColor = System.Drawing.Color.Black;
            this.gameView.Location = new System.Drawing.Point(11, 131);
            this.gameView.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.gameView.Name = "gameView";
            this.gameView.Size = new System.Drawing.Size(411, 411);
            this.gameView.TabIndex = 0;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 548);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbGameTime);
            this.Controls.Add(this.btnPlayGame);
            this.Controls.Add(this.gameView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "扫雷";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SweeperLibrary.GameView gameView;
        private System.Windows.Forms.Button btnPlayGame;
        private System.Windows.Forms.Label lbGameTime;
        private System.Windows.Forms.Label label2;
    }
}

