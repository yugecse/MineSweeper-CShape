﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SweeperLibrary
{
    public class CellBlockRole
    {
        /// <summary>
        /// 位于游戏地图中的坐标点X
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// 位于游戏地图中的坐标点Y
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// 是否展示最后格子所代表的结果
        /// </summary>
        public bool IsShowResult { get; set; } = false;

        /// <summary>
        /// 是否计算数字结果
        /// </summary>
        public bool IsComputeResult { get; set; } = false;

        /// <summary>
        /// 是否已经展示过计算结果了
        /// </summary>
        public bool IsHasShowComputed { get; set; } = false;

        /// <summary>
        /// 当前的格子的角色数字， -1：地雷，其他当前雷的数量
        /// </summary>
        public int Number { set; get; } = 0;

        /// <summary>
        /// 是否被Flag标识
        /// </summary>
        public bool IsFlag { get; set; } = false;

        /// <summary>
        /// 是否是雷
        /// </summary>
        public bool IsBoom => Number == -1;

    }
}
